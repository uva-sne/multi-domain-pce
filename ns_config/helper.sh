enter_frr () {
sudo nsenter --uts=/run/utsns/$1 --net=/run/netns/$1 vtysh -N  $1
}

stop_frr () { 
sudo nsenter --uts=/run/utsns/$1 /usr/lib/frr/frrinit.sh stop $1
}

start_frr () { 
sudo nsenter --uts=/run/utsns/$1 /usr/lib/frr/frrinit.sh start $1
}

restart_frr () { 
sudo nsenter --uts=/run/utsns/$1 /usr/lib/frr/frrinit.sh restart $1
}

pce1 () {
pushd /home/vagrant/pce-configs
echo "entering pce1"
sudo ip netns exec domain-1-pce1 /bin/bash --rcfile <(echo 'PS1="pce1 # "')
echo "exited pce1"
popd
}

pce2 () {
pushd /home/vagrant/pce-configs
echo "entering pce2"
sudo ip netns exec domain-2-pce2 /bin/bash --rcfile <(echo 'PS1="pce2 # "')
echo "exited pce2"
popd
}


