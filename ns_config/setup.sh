#!/bin/bash

set +ex
sudo bash utsns.sh
sudo bash domain1.sh
sudo bash domain2.sh
sudo bash d1-d2.sh 
sudo ip netns exec domain-1-pce1 bash domain1_bridge.sh 
sudo ip netns exec domain-2-pce2 bash domain2_bridge.sh 
sudo bash pce1-pce2.sh
