ip netns add domain-2-A
ip netns exec domain-2-A sysctl -w net.mpls.platform_labels=1048575
ip netns add domain-2-B
ip netns exec domain-2-B sysctl -w net.mpls.platform_labels=1048575
ip netns add domain-2-C
ip netns exec domain-2-C sysctl -w net.mpls.platform_labels=1048575
ip netns add domain-2-D
ip netns exec domain-2-D sysctl -w net.mpls.platform_labels=1048575
ip netns add domain-2-pce2

ip netns add host12

touch /etc/frr/domain-2-A/bfpd.conf
touch /etc/frr/domain-2-A/isisd.conf
touch /etc/frr/domain-2-A/ldpd.conf
touch /etc/frr/domain-2-A/nhrpd.conf
touch /etc/frr/domain-2-A/pathd.conf
touch /etc/frr/domain-2-A/ospfd.conf
touch /etc/frr/domain-2-A/ospf6d.conf
touch /etc/frr/domain-2-A/pimd.conf
touch /etc/frr/domain-2-A/ripd.conf
touch /etc/frr/domain-2-A/ripngd.conf
touch /etc/frr/domain-2-A/staticd.conf
touch /etc/frr/domain-2-A/vtysh.conf
touch /etc/frr/domain-2-A/zebra.conf
touch /etc/frr/domain-2-B/bfpd.conf
touch /etc/frr/domain-2-B/isisd.conf
touch /etc/frr/domain-2-B/ldpd.conf
touch /etc/frr/domain-2-B/nhrpd.conf
touch /etc/frr/domain-2-B/pathd.conf
touch /etc/frr/domain-2-B/ospfd.conf
touch /etc/frr/domain-2-B/ospf6d.conf
touch /etc/frr/domain-2-B/pimd.conf
touch /etc/frr/domain-2-B/ripd.conf
touch /etc/frr/domain-2-B/ripngd.conf
touch /etc/frr/domain-2-B/staticd.conf
touch /etc/frr/domain-2-B/vtysh.conf
touch /etc/frr/domain-2-B/zebra.conf
touch /etc/frr/domain-2-C/bfpd.conf
touch /etc/frr/domain-2-C/isisd.conf
touch /etc/frr/domain-2-C/ldpd.conf
touch /etc/frr/domain-2-C/nhrpd.conf
touch /etc/frr/domain-2-C/pathd.conf
touch /etc/frr/domain-2-C/ospfd.conf
touch /etc/frr/domain-2-C/ospf6d.conf
touch /etc/frr/domain-2-C/pimd.conf
touch /etc/frr/domain-2-C/ripd.conf
touch /etc/frr/domain-2-C/ripngd.conf
touch /etc/frr/domain-2-C/staticd.conf
touch /etc/frr/domain-2-C/vtysh.conf
touch /etc/frr/domain-2-C/zebra.conf
touch /etc/frr/domain-2-D/bfpd.conf
touch /etc/frr/domain-2-D/isisd.conf
touch /etc/frr/domain-2-D/ldpd.conf
touch /etc/frr/domain-2-D/nhrpd.conf
touch /etc/frr/domain-2-D/pathd.conf
touch /etc/frr/domain-2-D/ospfd.conf
touch /etc/frr/domain-2-D/ospf6d.conf
touch /etc/frr/domain-2-D/pimd.conf
touch /etc/frr/domain-2-D/ripd.conf
touch /etc/frr/domain-2-D/ripngd.conf
touch /etc/frr/domain-2-D/staticd.conf
touch /etc/frr/domain-2-D/vtysh.conf
touch /etc/frr/domain-2-D/zebra.conf
ip link add veth-A netns domain-2-pce2 type veth peer veth-pce2 netns domain-2-A
ip link add veth-B netns domain-2-pce2 type veth peer veth-pce2 netns domain-2-B
ip link add veth-C netns domain-2-pce2 type veth peer veth-pce2 netns domain-2-C
ip link add veth-D netns domain-2-pce2 type veth peer veth-pce2 netns domain-2-D
ip link add veth-A-B netns domain-2-A type veth peer veth-B-A netns domain-2-B
ip netns exec domain-2-A sysctl -w net.mpls.conf.veth-A-B.input=1
ip netns exec domain-2-B sysctl -w net.mpls.conf.veth-B-A.input=1
ip link add veth-A-D netns domain-2-A type veth peer veth-D-A netns domain-2-D
ip netns exec domain-2-A sysctl -w net.mpls.conf.veth-A-D.input=1
ip netns exec domain-2-D sysctl -w net.mpls.conf.veth-D-A.input=1
ip link add veth-B-C netns domain-2-B type veth peer veth-C-B netns domain-2-C
ip netns exec domain-2-B sysctl -w net.mpls.conf.veth-B-C.input=1
ip netns exec domain-2-C sysctl -w net.mpls.conf.veth-C-B.input=1
ip link add veth-C-D netns domain-2-C type veth peer veth-D-C netns domain-2-D
ip netns exec domain-2-C sysctl -w net.mpls.conf.veth-C-D.input=1
ip netns exec domain-2-D sysctl -w net.mpls.conf.veth-D-C.input=1

ip link add veth-host12-D netns host12 type veth peer veth-D-host12 netns domain-2-D
ip netns exec host12 ip addr add 192.168.2.1/30 dev veth-host12-D
ip netns exec host12 ip link set veth-host12-D up
ip netns exec host12 ip route add default via 192.168.2.2 dev veth-host12-D
ip netns exec domain-2-D ip addr add 192.168.2.2/30 dev veth-D-host12
ip netns exec domain-2-D ip link set veth-D-host12 up

touch /run/utsns/domain-2-A
unshare --uts=/run/utsns/domain-2-A hostname domain-2-A
touch /run/utsns/domain-2-B
unshare --uts=/run/utsns/domain-2-B hostname domain-2-B
touch /run/utsns/domain-2-C
unshare --uts=/run/utsns/domain-2-C hostname domain-2-C
touch /run/utsns/domain-2-D
unshare --uts=/run/utsns/domain-2-D hostname domain-2-D
nsenter --uts=/run/utsns/domain-2-A /usr/lib/frr/frrinit.sh start domain-2-A
nsenter --uts=/run/utsns/domain-2-B /usr/lib/frr/frrinit.sh start domain-2-B
nsenter --uts=/run/utsns/domain-2-C /usr/lib/frr/frrinit.sh start domain-2-C
nsenter --uts=/run/utsns/domain-2-D /usr/lib/frr/frrinit.sh start domain-2-D
