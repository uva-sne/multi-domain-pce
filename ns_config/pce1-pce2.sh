ip link add veth-pce2 netns domain-1-pce1 type veth peer veth-pce1 netns domain-2-pce2 
ip netns exec domain-1-pce1 ip addr add 10.101.0.1/30 dev veth-pce2
ip netns exec domain-1-pce1 ip link set veth-pce2 up 
ip netns exec domain-2-pce2 ip addr add 10.101.0.2/30 dev veth-pce1
ip netns exec domain-2-pce2 ip link set veth-pce1 up 

ip netns exec domain-2-pce2 ip route add 10.203.0.1/32 via 10.101.0.2 dev veth-pce1
ip netns exec domain-1-pce1 ip route add 10.223.0.1/32 via 10.101.0.1 dev veth-pce2
