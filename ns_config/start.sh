#!/bin/sh

tmux new-session -d -s demo 
tmux rename-window 'pce1'
tmux split-window -v  
tmux split-window -h 
tmux select-pane -t 0
tmux split-window -h 

tmux send-keys -t 0 'source helper.sh; pce1' C-m './d1.sh' C-m
tmux send-keys -t 1 'source helper.sh; pce2' C-m './d2.sh' C-m
tmux send-keys -t 2 'source helper.sh; enter_frr domain-1-A' C-m
tmux send-keys -t 3 'source helper.sh; enter_frr domain-2-B' C-m 

tmux -2 attach-session -t demo
