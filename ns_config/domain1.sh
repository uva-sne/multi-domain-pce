ip netns add domain-1-A
ip netns exec domain-1-A sysctl -w net.mpls.platform_labels=1048575
ip netns add domain-1-B
ip netns exec domain-1-B sysctl -w net.mpls.platform_labels=1048575
ip netns add domain-1-C
ip netns exec domain-1-C sysctl -w net.mpls.platform_labels=1048575
ip netns add domain-1-D
ip netns exec domain-1-D sysctl -w net.mpls.platform_labels=1048575
ip netns add domain-1-pce1

ip netns add host11

touch /etc/frr/domain-1-A/bfpd.conf
touch /etc/frr/domain-1-A/isisd.conf
touch /etc/frr/domain-1-A/ldpd.conf
touch /etc/frr/domain-1-A/nhrpd.conf
touch /etc/frr/domain-1-A/pathd.conf
touch /etc/frr/domain-1-A/ospfd.conf
touch /etc/frr/domain-1-A/ospf6d.conf
touch /etc/frr/domain-1-A/pimd.conf
touch /etc/frr/domain-1-A/ripd.conf
touch /etc/frr/domain-1-A/ripngd.conf
touch /etc/frr/domain-1-A/staticd.conf
touch /etc/frr/domain-1-A/vtysh.conf
touch /etc/frr/domain-1-A/zebra.conf
touch /etc/frr/domain-1-B/bfpd.conf
touch /etc/frr/domain-1-B/isisd.conf
touch /etc/frr/domain-1-B/ldpd.conf
touch /etc/frr/domain-1-B/nhrpd.conf
touch /etc/frr/domain-1-B/pathd.conf
touch /etc/frr/domain-1-B/ospfd.conf
touch /etc/frr/domain-1-B/ospf6d.conf
touch /etc/frr/domain-1-B/pimd.conf
touch /etc/frr/domain-1-B/ripd.conf
touch /etc/frr/domain-1-B/ripngd.conf
touch /etc/frr/domain-1-B/staticd.conf
touch /etc/frr/domain-1-B/vtysh.conf
touch /etc/frr/domain-1-B/zebra.conf
touch /etc/frr/domain-1-C/bfpd.conf
touch /etc/frr/domain-1-C/isisd.conf
touch /etc/frr/domain-1-C/ldpd.conf
touch /etc/frr/domain-1-C/nhrpd.conf
touch /etc/frr/domain-1-C/pathd.conf
touch /etc/frr/domain-1-C/ospfd.conf
touch /etc/frr/domain-1-C/ospf6d.conf
touch /etc/frr/domain-1-C/pimd.conf
touch /etc/frr/domain-1-C/ripd.conf
touch /etc/frr/domain-1-C/ripngd.conf
touch /etc/frr/domain-1-C/staticd.conf
touch /etc/frr/domain-1-C/vtysh.conf
touch /etc/frr/domain-1-C/zebra.conf
touch /etc/frr/domain-1-D/bfpd.conf
touch /etc/frr/domain-1-D/isisd.conf
touch /etc/frr/domain-1-D/ldpd.conf
touch /etc/frr/domain-1-D/nhrpd.conf
touch /etc/frr/domain-1-D/pathd.conf
touch /etc/frr/domain-1-D/ospfd.conf
touch /etc/frr/domain-1-D/ospf6d.conf
touch /etc/frr/domain-1-D/pimd.conf
touch /etc/frr/domain-1-D/ripd.conf
touch /etc/frr/domain-1-D/ripngd.conf
touch /etc/frr/domain-1-D/staticd.conf
touch /etc/frr/domain-1-D/vtysh.conf
touch /etc/frr/domain-1-D/zebra.conf
ip link add veth-A netns domain-1-pce1 type veth peer veth-pce1 netns domain-1-A
ip link add veth-B netns domain-1-pce1 type veth peer veth-pce1 netns domain-1-B
ip link add veth-C netns domain-1-pce1 type veth peer veth-pce1 netns domain-1-C
ip link add veth-D netns domain-1-pce1 type veth peer veth-pce1 netns domain-1-D
ip link add veth-A-B netns domain-1-A type veth peer veth-B-A netns domain-1-B
ip netns exec domain-1-A sysctl -w net.mpls.conf.veth-A-B.input=1
ip netns exec domain-1-B sysctl -w net.mpls.conf.veth-B-A.input=1
ip link add veth-A-D netns domain-1-A type veth peer veth-D-A netns domain-1-D
ip netns exec domain-1-A sysctl -w net.mpls.conf.veth-A-D.input=1
ip netns exec domain-1-D sysctl -w net.mpls.conf.veth-D-A.input=1
ip link add veth-B-C netns domain-1-B type veth peer veth-C-B netns domain-1-C
ip netns exec domain-1-B sysctl -w net.mpls.conf.veth-B-C.input=1
ip netns exec domain-1-C sysctl -w net.mpls.conf.veth-C-B.input=1
ip link add veth-C-D netns domain-1-C type veth peer veth-D-C netns domain-1-D
ip netns exec domain-1-C sysctl -w net.mpls.conf.veth-C-D.input=1
ip netns exec domain-1-D sysctl -w net.mpls.conf.veth-D-C.input=1

ip link add veth-host11-A netns host11 type veth peer veth-A-host11 netns domain-1-A
ip netns exec host11 ip addr add 192.168.1.1/30 dev veth-host11-A
ip netns exec host11 ip link set veth-host11-A up
ip netns exec host11 ip route add default via 192.168.1.2 dev veth-host11-A
ip netns exec domain-1-A ip addr add 192.168.1.2/30 dev veth-A-host11
ip netns exec domain-1-A ip link set veth-A-host11 up

touch /run/utsns/domain-1-A
unshare --uts=/run/utsns/domain-1-A hostname domain-1-A
touch /run/utsns/domain-1-B
unshare --uts=/run/utsns/domain-1-B hostname domain-1-B
touch /run/utsns/domain-1-C
unshare --uts=/run/utsns/domain-1-C hostname domain-1-C
touch /run/utsns/domain-1-D
unshare --uts=/run/utsns/domain-1-D hostname domain-1-D
nsenter --uts=/run/utsns/domain-1-A /usr/lib/frr/frrinit.sh start domain-1-A
nsenter --uts=/run/utsns/domain-1-B /usr/lib/frr/frrinit.sh start domain-1-B
nsenter --uts=/run/utsns/domain-1-C /usr/lib/frr/frrinit.sh start domain-1-C
nsenter --uts=/run/utsns/domain-1-D /usr/lib/frr/frrinit.sh start domain-1-D
