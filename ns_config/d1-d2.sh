



#domain2-B
#interface veth-D2B-D1D
# ip address 10.100.0.2/30
#exit
#
#domain1-D
#interface veth-D1D-D2B
# ip address 10.100.0.1/30
#exit
#

ip link add veth-D2B-D1D netns domain-2-B type veth peer veth-D1D-D2B netns domain-1-D
ip netns exec domain-2-B sysctl -w net.mpls.conf.veth-D2B-D1D.input=1
ip netns exec domain-1-D sysctl -w net.mpls.conf.veth-D1D-D2B.input=1

ip netns exec domain-2-B ip addr add 10.100.0.2/30 dev veth-D2B-D1D
ip netns exec domain-1-D ip addr add 10.100.0.1/30 dev veth-D1D-D2B

