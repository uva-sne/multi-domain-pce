#!/bin/bash 

sudo apt update
sudo apt-get install -y kitty-terminfo locales-all tmux vim
sudo apt-get install -y openjdk-11-jdk-headless maven
sudo apt-get install -y build-essential

sudo apt-get install -y git autoconf automake libtool make \
  libreadline-dev texinfo libjson-c-dev pkg-config bison flex \
  libc-ares-dev python3-dev python3-pytest python3-sphinx build-essential \
  libsnmp-dev libcap-dev libelf-dev libunwind-dev

sudo apt-get install -y libgrpc++-dev libgrpc-dev protobuf-c-compiler protobuf-compiler libprotoc-dev grpc-proto protobuf-compiler-grpc

sudo apt-get install -y libpcre2-dev libpcre2-posix2

if [ `lsb_release -is` = "Ubuntu" ]; then
	sudo apt-get install -y linux-generic libgrpc-dev protobuf-compiler-grpc libprotobuf-c-dev libgrpc++-dev
	wget -r -l 1 -e robots=off https://ci1.netdef.org/artifact/LIBYANG-LIBYANGV2/shared/build-5/Ubuntu-20.04-x86_64-Packages
	cd ci1.netdef.org/artifact/LIBYANG-LIBYANGV2/shared/build-5/Ubuntu-20.04-x86_64-Packages
else
	# Assume Debian
	wget -r -l 1 -e robots=off https://ci1.netdef.org/artifact/LIBYANG-LIBYANGV2/shared/build-5/Debian-11-x86_64-Packages
	cd ci1.netdef.org/artifact/LIBYANG-LIBYANGV2/shared/build-5/Debian-11-x86_64-Packages
fi

sudo dpkg -i *.deb

cd ~

sudo bash /vagrant/create_users.sh
git clone https://bitbucket.org/uva-sne/frr
cd frr
bash /vagrant/build_frr.sh


export JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64/"
cd ~
git clone https://bitbucket.org/uva-sne/netphony-pce
git clone https://bitbucket.org/uva-sne/netphony-topology/
git clone https://bitbucket.org/uva-sne/netphony-network-protocols/

cd netphony-network-protocols
mvn package
mvn install
cd ../netphony-topology
mvn install
cd ../netphony-pce
mvn  package -P generate-autojar-Multi-PCE
cd ~

cp -r /vagrant/pce_config /home/vagrant/pce-configs
cp -r /vagrant/ns_config /home/vagrant/ns_config

sudo cp -f /vagrant/etc/motd /etc/motd

sudo cp -r /vagrant/frr_config/* /etc/frr
sudo chown -R frr:frr /etc/frr
sudo chown -R frr:frrvty /etc/frr/vtysh.conf 


sudo sh -c "cat /vagrant/etc/sysctl.conf >> /etc/sysctl.conf"
sudo sh -c "cat /vagrant/etc/modules.conf >> /etc/modules"

for module in `cat /vagrant/etc/modules.conf`; do 
	sudo modprobe $module; 
done

sudo sysctl -p 
