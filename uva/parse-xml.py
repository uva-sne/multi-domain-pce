import sys
import networkx as nx
from jinja2 import Template
import ipaddress

zebra_template = """
{%- for interface in interfaces %}
interface {{ interface["name"] -}}
 ip address {{uinterface["address"] }}
!
{% endfor %}
"""

class Domain:
    global_counter = 1
    services = ["bfpd","isisd","ldpd","nhrpd","pathd","ospfd","ospf6d","pimd","ripd",
                "ripngd","staticd","vtysh","zebra"]
    frr_path_prefix = "/etc/frr/"
    zebra_template = """
{%- for interface in interfaces %}
interface {{ interface["name"] }}
 ip address {{ interface["address"] }}
 ip ospf network point-to-point
 ip router isis 1
 isis network point-to-point
!
{% endfor %}
"""
    def __init__(self, topology):
        self.G = topology
        self.counter = self.global_counter
        Domain.global_counter += 1
        self.prefix = f"domain-{self.counter}"
        self.p2p_networks = ipaddress.ip_network(f"10.{self.counter}.0.0/24").subnets(new_prefix=30)
        self.prefixes = dict()
        self.template = Template(self.zebra_template)

    def print_ns(self):
        for node in self.G.nodes:
            print(f"ip netns add {self.prefix}-{node}")
            print(f"ip netns exec {self.prefix}-{node} sysctl -w net.mpls.platform_labels=1048575")
            self.prefixes[node]= list()

        print(f"ip netns add {self.prefix}-pce{self.counter}")

        for node in self.G.nodes:
            for service in self.services:
                print(f"touch {self.frr_path_prefix}{self.prefix}-{node}/{service}.conf")

        for node in G.nodes:
            print(f"ip link add veth-{node} netns {self.prefix}-pce{self.counter} " +
                  f"type veth peer veth-pce{self.counter} netns {self.prefix}-{node}")

        for edge in G.edges:
            side_a = edge[0] +"-" +edge[1]
            side_b = edge[1] +"-" +edge[0]
            print(f"ip link add veth-{side_a} netns {self.prefix}-{edge[0]} type veth " +
                  f"peer veth-{side_b} netns {self.prefix}-{edge[1]}")
            print(f"ip netns exec {self.prefix}-{edge[0]} sysctl -w net.mpls.conf.veth-{side_a}.input=1")
            print(f"ip netns exec {self.prefix}-{edge[1]} sysctl -w net.mpls.conf.veth-{side_b}.input=1")
            link = next(self.p2p_networks).hosts()
            interface = { "name": f"veth-{side_a}", "address": str(next(link))+"/30"}
            self.prefixes[edge[0]].append(interface)
            interface = { "name": f"veth-{side_b}", "address": str(next(link))+"/30"}
            self.prefixes[edge[1]].append(interface)

    def print_destroy_ns(self):
        for node in self.G.nodes:
            print(f"ip netns delete {self.prefix}-{node}")

        print(f"ip netns delete {self.prefix}-pce{self.counter}")
    
    def print_create_uts(self):
        for node in self.G.nodes:
            print(f"touch /run/utsns/{self.prefix}-{node}")
            print(f"unshare --uts=/run/utsns/{self.prefix}-{node} hostname {self.prefix}-{node}")

    def print_frrinit(self):
        for node in self.G.nodes:
            print(f"nsenter --uts=/run/utsns/{self.prefix}-{node} /usr/lib/frr/frrinit.sh start {self.prefix}-{node}")
    def render_zebra(self):
        for node in self.prefixes:
            print(f"====== {node} =====")
            print(self.template.render(interfaces=self.prefixes[node]))


G = nx.Graph()
G.add_edge("A","B")
G.add_edge("B","C")
G.add_edge("C","D")
G.add_edge("D","A")

domain_1 = Domain(G)
domain_2 = Domain(G)

domain_2.print_ns()
domain_2.print_create_uts()
domain_2.render_zebra()
domain_2.print_frrinit()
domain_2.print_destroy_ns()
