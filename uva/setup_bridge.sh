ip link add name br0 type bridge
ip link set veth-A up
ip link set veth-B up
ip link set veth-C up
ip link set veth-D up
ip link set veth-A master br0
ip link set veth-B master br0
ip link set veth-C master br0
ip link set veth-D master br0

ip address add dev br0 10.223.0.1/24
ip link set br0 up
ip route
ip link set br0 up

ip route add 10.0.1.1/32 via 10.223.0.101
ip route add 10.0.1.2/32 via 10.223.0.102
ip route add 10.0.1.3/32 via 10.223.0.103
ip route add 10.0.1.4/32 via 10.223.0.104

