# What?

Configures an environment to experiment with a multi-domain-pce. 

Its similar to and based on: 
https://bitbucket.org/multi-domain-pce/

Except that this configuration uses linux namespaces instead of vms and IS-IS as an IGP.


# How to use?

First install:

- virtualbox https://www.virtualbox.org/
- vagrant https://www.vagrantup.com/

Then:
```
  git clone https://bitbucket.org/uva-sne/multi-domain-pce/
  cd multi-domain-pce
  vagrant up
```

Go for a coffee since it takes a while to compile and then: 
```
  vagrant ssh
```

Inside the vm:
```
  cd ns_config
  ./setup.sh
  ./start.sh
```

start.sh shows a 2x2 grid using tmux the top 2 panes show PCE1 and PCE2 and the bottom two panes show domain-1-A and domain-2-B.

To navigate the panes use CTRL-B-[arrow key] a tmux cheat sheet can be found at: https://acloudguru.com/blog/engineering/tmux-cheat-sheet
